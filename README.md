I'm writing these comments several years after using the script - so take
them a guidance but they may not be accurate.

This script is for downloading public comments in bulk from the Barnet Planning
website - for better analysis of them (e.g. locations of objections/support).

If I remember correctly the PDF method was the most successful one in the end.