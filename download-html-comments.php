<?php
/**
 * This file gets comments from the Barnet Planning Website via the HTML interface.
 * 
 * I don't remember (I'm writing this comment years later) whether the HTML and PDF
 * comments are different, or just to ways to access the same.
 * 
 * Edit the $get variable below, then run this script on the command line:
 * 
 * php download-html-comments.php
 * 
 * to download the comments into $outputCsv
 */
use Asika\Pdf2text;
use Goutte\Client;
use Concat\Http\Handler\CacheHandler;
use Doctrine\Common\Cache\FilesystemCache;

require_once __DIR__.'/vendor/autoload.php';

$client = new Client();

$get = [
    'activeTab' => 'neighbourComments',
    'neighbourCommentsPager.page' => 61,
    'keyVal' => 'OF72J8JI0BO00',
];

$url = 'https://publicaccess.barnet.gov.uk/online-applications/applicationDetails.do';

$orderByStringOptions = [
    'Added',
    'Name',
];

$orderByDirectionStringOptions = [
    'Ascending',
    'Descending',
];

// Output the CSV for output
$dataDir = __DIR__.'/data/';
$outputCsv = $dataDir.'planning-comments.csv';
$fileIsNew = ! file_exists($outputCsv);
$fp = fopen($outputCsv, 'w');

foreach ($orderByStringOptions as $orderByString) {
    foreach ($orderByDirectionStringOptions as $orderByDirectionString) {
        // Page Crawler represents each page. $crawler represens the overall
        // crawl effort. PageCrawler is used for reading comments and $crawler
        // is used for changing sort options. $crawler is static, $pageCrawler
        // changes each page loop.
        $crawler = $client->request('GET', $url.'?'.http_build_query($get));

        // Specify our sorting preferences
        $sortForm = $crawler->selectButton('Go')->form();
        $pageCrawler = $client->submit($sortForm, [
            'neighbourCommentsPager.orderByString' => $orderByString,
            'neighbourCommentsPager.orderByDirectionString' => $orderByDirectionString,
            'neighbourCommentsPager.resultsPerPage' => '10',
        ]);

        // Keep going as long as we have comments
        $haveComments = true;
        while ($haveComments) {
            $pageNumber = $pageCrawler->filter('.pager.top > strong')->text();
            echo "Page $pageNumber ($orderByString $orderByDirectionString)\n";
            $comments = $pageCrawler->filter('.comment');
            if (count($comments) === 0) {
                $haveComments = false;
                continue;
            }

            $comments->each(function ($node) use ($fp) {
                $comment = [
                    'name' => $node->filter('.consultationName')->text(),
                    'address' => $node->filter('.consultationAddress')->text(),
                    'stance' => $node->filter('.consultationStance')->text(),
                ];
                $comment['name'] = trim($comment['name']);
                $comment['address'] = trim($comment['address']);
                $comment['stance'] = trim(str_replace(['(',')'], '', $comment['stance']));

                fputcsv(
                    $fp,
                    array(
                        $comment['name'],
                        $comment['address'],
                        '',
                        $comment['stance'],
                        '',
                        '',
                    )
                );
            });
            $nextPage = $pageCrawler->selectLink('Next')->link();
            $pageCrawler = $client->click($nextPage);
        }
    }
}

fclose($fp);
