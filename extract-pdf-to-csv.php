<?php

use Asika\Pdf2text;

require_once __DIR__.'/vendor/autoload.php';

class PlanningComment
{
    private $isParsed = false;
    private $customer;
    private $comment;
    private $lines;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function parse()
    {
        if ($this->isParsed) {
            return;
        }
    }

    private function getLines()
    {
        if (!$this->lines) {
            $this->lines = explode("\n", $this->text);
        }
        return $this->lines;
    }

    /**
     * Returns true if the line is a heading. If the second param is passed
     * then returns true if it is that heading
     *
     * @param  [type] $line
     * @param  [type] $heading
     *
     * @return [type]          [description]
     */
    private function lineIsHeading($line, $heading = false)
    {
        $line = trim(strtolower($line));
        $knownHeadings = [
            'application summary',
            'customer details',
            'comment details'
        ];
        if (isset($heading)) {
            return $line === trim(strtolower($heading));
        }
        if (in_array($line, $knownHeadings)) {
            return true;
        }
    }

    public function getSection($sectionName)
    {
        $lines = [];
        $onChosenSection = false;
        // Go through each line
        foreach ($this->getLines() as $line) {
            // If the line says "Customer Details" we're in the CD section
            if ($this->lineIsHeading($line, $sectionName)) {
                $onChosenSection = true;
                continue;
            } elseif ($this->lineIsHeading($line)) {
                // If we're not that heading, but we are another heading - we've left
                // the CD section
                $onChosenSection = false;
            }
            if ($onChosenSection) {
                $lines[] = $line;
            }
        }
        $return = [];
        foreach ($lines as $line) {
            $parts = explode(':', $line);
            // If there's only one part, but we have a part name from the last
            // loop append the value.
            if (isset($partName) && count($parts) === 1) {
                $return[$partName] .= ' '.trim($parts[0]);
            } elseif (count($parts) == 2) {
                $partName = strtolower(trim($parts[0]));
                $return[$partName] = trim($parts[1]);
            }
        }
        return $return;
    }

    public function getCustomer()
    {
        if (!$this->customer) {
            $this->customer = $this->getSection('customer details');
        }
        return $this->customer;
    }

    public function getComment()
    {
        if (!$this->comment) {
            $this->comment = $this->getSection('comment details');
        }
        return $this->comment;
    }

    public function getStance()
    {
        $isObjection = (false !== strpos($this->getComment()['stance'], 'objects'));
        return $isObjection ? 'against' : 'for';
    }

    public function getPostcode()
    {
        $postcodeRegex = '#^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$#';
       // The UK post code is valid according to your criteria
        $address = $this->getCustomer()['address'];
        $matches = [];
        preg_match($postcodeRegex, $address, $matches);
        var_dump($matches);
    }
}

// Parse all the PDF files
$dataDir = __DIR__.'/data/';
$outputCsv = $dataDir.'planning-comments.csv';
$fileIsNew = ! file_exists($outputCsv);

$fp = fopen($outputCsv, 'w');

// If the file is new, add the headings
if ($fileIsNew) {
	fputcsv(
	    $fp,
	    array(
	        'customer_name',
	        'customer_address',
	        'customer_postcode',
	        'stance',
	        'commenter_type',
	        'comment',
	    )
	);
}

foreach (glob($dataDir.'*.pdf') as $pdfFile) {
    echo "Processing: ".basename($pdfFile)."\n";
    // Parse pdf file and build necessary objects.
    $parser = new \Smalot\PdfParser\Parser();
    $pdf    = $parser->parseFile($pdfFile);
     
    $text = $pdf->getText();
    $comment = new PlanningComment($text);
    $customerData = $comment->getCustomer();
    $commentData = $comment->getComment();
    fputcsv(
        $fp,
        array(
            $customerData['name'],
            $customerData['address'],
            'unknown',
            $comment->getStance(),
            $commentData['commenter type'],
            $commentData['comment'],
        )
    );
}

fclose($fp);
