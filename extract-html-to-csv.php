<?php

use Asika\Pdf2text;

require_once __DIR__.'/vendor/autoload.php';

class PlanningComment
{
    private $isParsed = false;
    private $customer;
    private $comment;
    private $lines;

    public function __construct($text)
    {
        $this->text = $text;
    }

    private function regexHtml($regex)
    {
        $text = str_replace(["\n", "\r"], '', $this->text);
        $matches = [];
        preg_match($regex, $text, $matches);
        if (isset($matches[1])) {
            return $matches[1];
        } else {
            return 'unknown';
        }
    }

    public function getCustomerName() {
        return $this->regexHtml('/consultationName\"\>\s*(.*?)\s*</');
     }

    public function getCustomerAddress() {
        return $this->regexHtml('/consultationAddress\"\>\s*(.*?)\s*</');
     }

    public function getCustomer()
    {
        return [
            'name' => $this->getCustomerAddress(),
            'address' => $this->getCustomerAddress(),
        ];
    }

    public function getComment()
    {
        return [
            'commenter type' => 'none',
            'comment' => 'none',
        ];
    }

    public function getStance()
    {
        $stance = $this->regexHtml('/consultationStance\"\>\s*(.*?)\s*</');
        $stance = str_replace(['(', ')'], '', $stance);
        return $stance;
    }

    public function getCustomerPostcode()
    {
        $postcodeRegex = '/([A-z]{1,3}[0-9]{1,3}[A-z]?\s?[0-9]{1,3}[A-z]{1,3})/';
       // The UK post code is valid according to your criteria
        $matches = [];
        preg_match($postcodeRegex, $this->getCustomerAddress(), $matches);
        if (isset($matches[1])) {
            return $matches[1];
        } else {
            return 'unknown';
        }
    }
}

// Parse all the PDF files
$dataDir = __DIR__.'/data/';
$outputCsv = $dataDir.'planning-comments.csv';
$fileIsNew = ! file_exists($outputCsv);

$fp = fopen($outputCsv, 'a');

// If the file is new, add the headings
if ($fileIsNew) {
    fputcsv(
        $fp,
        array(
            'customer_name',
            'customer_address',
            'customer_postcode',
            'stance',
            'commenter_type',
            'comment',
        )
    );
}

foreach (glob($dataDir.'*.html') as $htmlFile) {
    echo "Processing: ".basename($htmlFile)."\n";
    // Open the HTML file and build necessary objects.
    $text = file_get_contents($htmlFile);
    $comment = new PlanningComment($text);
    $customerData = $comment->getCustomer();
    $commentData = $comment->getComment();
    fputcsv(
        $fp,
        array(
            $comment->getCustomerName(),
            $comment->getCustomerAddress(),
            $comment->getCustomerPostcode(),
            $comment->getStance(),
            $commentData['commenter type'],
            $commentData['comment'],
        )
    );
}

fclose($fp);
