<?php
/**
 * Download all the comments by PDF - saves a copy of the PDF in data/
 *
 * Each PDF contains one comment. Then use extract-pdf-to-csv to parse them.
 */
use Asika\Pdf2text;
use Goutte\Client;
use Concat\Http\Handler\CacheHandler;
use Doctrine\Common\Cache\FilesystemCache;

require_once __DIR__.'/vendor/autoload.php';

$cacheProvider = new FilesystemCache(__DIR__ . '/cache');

$client = new Client();
// Get a cookie and create a context for file operations
$anyFastLoadingPage = 'https://publicaccess.barnet.gov.uk/online-applications/applicationDetails.do?activeTab=summary&keyVal=OF72J8JI0BO00';
$crawler = $client->request('GET', $anyFastLoadingPage);
$sessionCookie = $client->getCookieJar()->allValues($anyFastLoadingPage)['JSESSIONID'];
$context = stream_context_create(array(
  'http'=>array(
    'method'=>"GET",
    'header'=>"Accept-language: en\r\n" .
              "Cookie: JSESSIONID={$sessionCookie}\r\n"
  )
));

// Get the latest version of the "All Comments" page
$url = 'https://publicaccess.barnet.gov.uk/online-applications/applicationDetails.do?activeTab=documents&keyVal=OF72J8JI0BO00&filterType=documentType&documentType=Public%20Comment&resetFilter=false';
if (!file_exists('w:\www\cantheyeat\public\planning-scraper.html')) {
    copy($url, 'w:\www\cantheyeat\public\planning-scraper.html', $context);
}
$url = 'http://local.cantheyeat/planning-scraper.html';
$crawler = $client->request('GET', $url);

// Download all the PDF files
$dataDir = __DIR__.'/data/';
foreach ($crawler->filter('#Documents a') as $node) {
    $url = 'https://publicaccess.barnet.gov.uk'.$node->getAttribute('href');
    echo $url."\n";
    $filename = basename($url);
    $localFile = $dataDir.$filename;
    if (!file_exists($localFile)) {
        copy($url, $localFile, $context);
    }

}
// $reader = new \Asika\Pdf2text;
// $output = $reader->decode($fileName);
